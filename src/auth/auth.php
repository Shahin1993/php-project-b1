<?php

namespace App\auth;
include("../../vendor/autoload.php");
use App\utility\utility;
use PDO;
session_start();
class auth
{
  private $username;
  private $email;
  private $password;

  public function setData($data='')
{
   $this->username=$data['username'];
   $this->email=$data['email'];
   $this->password=$data['password'];
   return $this;
}
public function store()
{
   try{
       $db=new PDO('mysql:host=localhost;dbname=bitm_1','root','');
       $query="INSERT INTO user_tbl(username,email,password) values(:u, :e, :p)";
       $stmt=$db->prepare($query);
       $status=$stmt->execute(
           array(
               ':u'=>$this->username,
               ':e'=>$this->email,
               ':p'=>$this->password,
       ));
       if($status){
           $_SESSION['Message']="Welcome for sign up";

           header("location:create.php");
       }
       else{
           echo "something is wrong";
       }

   }
   catch (PDOException $e)
   {
       echo 'Error'.$e->getMessage();
   }
}
}
?>